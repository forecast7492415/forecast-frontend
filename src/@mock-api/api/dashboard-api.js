import mock from "../mock";
// import mockApi from "../data/plant-data.json";

// let powerPlants = mockApi.powerPlants;

// /* eslint-disable camelcase */
// mock.onGet('api/user/dashboard/findAll').reply((config) => {
//     let numOfPlants = config.data || powerPlants.length;
//     return [200, powerPlants.slice(0, numOfPlants)];
// });

// mock.onGet('api/user/dashboard/findPlantsByIds').reply((config) => {
//     const ids = (JSON.parse(config.data)).ids.map(id => parseInt(id));
//     const filteredProducts = powerPlants.filter(product => ids.includes(product.id));
//     return [200, filteredProducts];
// });

mock.onGet("/currentMeteoInfo").reply(() => {
  const currentClouds = Math.random() * 10;
  const currentIrradiation = Math.random() * 1000;
  const currentTemp = Math.random() * 100 - 50;
  const currentWind = Math.random() * 100;

  return [200, { currentClouds, currentIrradiation, currentTemp, currentWind }];
});

// mock.onGet("/api/v1/meteo/day-stats").reply(() => {
//   function generateRandomValue(range, base = 0, variance = 0) {
//     const actual = base + Math.random() * range;
//     const prediction = actual + (Math.random() - 0.5) * variance;
//     return { actual, prediction };
//   }
//
//   function generateRandomData() {
//     const data = [];
//
//     for (let i = 0; i < 24; i++) {
//       const labels = i;
//       const windSpeed = generateRandomValue(20, 0, 5);
//       const windDirection = generateRandomValue(360);
//       const airDensity = generateRandomValue(0.05, 1.225, 0.02);
//       const airTemperature = generateRandomValue(40, -10, 5);
//       const atmosphericPressure = generateRandomValue(5000, 101325, 1000);
//       const precipitationProbability = Math.random() < 0.3;
//       const precipitation = precipitationProbability
//         ? generateRandomValue(10)
//         : { actual: 0, prediction: 0 };
//
//       data.push({
//         windSpeed,
//         windDirection,
//         airDensity,
//         airTemperature,
//         atmosphericPressure,
//         precipitation,
//       });
//     }
//
//     return data;
//   }
//
//   const sampleData = generateRandomData();
//   return [200, sampleData];
// });

mock.onGet("dashboard/ping").reply((config) => {
  const { dateStart } = config.params;
  if (dateStart) {
    let dataTemplate = {
      prognozfakt_graph_dt: [],
      prognozfakt_graph_fc: [],
      prognozfakt_graph_his: [],
      meteo_t_graph_fc: [],
      meteo_t_graph_his: [],
      meteo_c_graph_fc: [],
      meteo_c_graph_his: [],
      meteo_h_graph_fc: [],
      meteo_h_graph_his: [],
      meteo_w_graph_fc: [],
      meteo_w_graph_his: [],
      totalProduceIncome: 0,
      totalCoReduction: 0,
      prognozfakt: "NaN",
      power_graph_his: 0.0,
      kium_graph_his: "NaN",
    };

    let getTimeDifferenceResult = getTimeDifference(dateStart);
    dataTemplate.prognozfakt_graph_dt = getTimeDifferenceResult;

    // Fill dataTemplate with random data
    for (let i = 0; i < getTimeDifferenceResult.length; i++) {
      dataTemplate.prognozfakt_graph_fc.push(Math.random() * 100);
      dataTemplate.prognozfakt_graph_his.push(Math.random() * 100);
      dataTemplate.meteo_t_graph_fc.push(Math.random() * 100);
      dataTemplate.meteo_t_graph_his.push(Math.random() * 100);
      dataTemplate.meteo_c_graph_fc.push(Math.random() * 100);
      dataTemplate.meteo_c_graph_his.push(Math.random() * 100);
      dataTemplate.meteo_h_graph_fc.push(Math.random() * 100);
      dataTemplate.meteo_h_graph_his.push(Math.random() * 100);
      dataTemplate.meteo_w_graph_fc.push(Math.random() * 100);
      dataTemplate.meteo_w_graph_his.push(Math.random() * 100);
    }

    // fill totalProduceIncome and totalCoReduction...
    dataTemplate.totalProduceIncome = Math.random() * 100;
    dataTemplate.totalCoReduction = Math.random() * 100;
    dataTemplate.prognozfakt = Math.random() * 100;
    dataTemplate.power_graph_his = Math.random() * 100;
    dataTemplate.kium_graph_his = Math.random() * 100;

    return [200, dataTemplate];
  } else {
    return [400, { error: "dateStart and dateEnd are required" }];
  }
});

// ----------------------------------------------------------------------
function getTimeDifference(dateStart) {
  const start = new Date(`${dateStart}T00:00:00Z`);
  const difference = Math.round(start / (1000 * 60 * 60 * 24)) + 1;

  let current = start;
  const result = [];
  for (let i = 0; i < difference * 24; i++) {
    result.push(current.toISOString().slice(0, 19).replace("T", " "));
    current.setHours(current.getHours() + 1);
  }
  return result;
}
