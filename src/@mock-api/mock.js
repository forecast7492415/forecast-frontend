import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';

const mock = new MockAdapter(axios, { delayResponse: 500, onNoMatch: 'passthrough' });
export default mock;
