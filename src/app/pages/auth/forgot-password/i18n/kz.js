const locale = {
  TITLE: "Құпия сөзді ұмыттыңыз ба?",
  DESCRIPTION: "Құпия сөзді қалпына келтіру формасын толтырыңыз",
  EMAIL_PLACEHOLDER: "Email-ді енгізіңіз",
  SEND_LINK: "Сілтеме жіберу",
  FOOTER: "Аккаунтыңыз бар ма?",
  SIGN_IN: "Кіру",
};
export default locale;
