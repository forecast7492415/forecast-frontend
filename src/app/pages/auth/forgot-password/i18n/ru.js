const locale = {
  TITLE: "Забыли пароль?",
  DESCRIPTION: "Заполните форму сброса пароля",
  EMAIL_PLACEHOLDER: "Введите email",
  SEND_LINK: "Отправить ссылку",
  FOOTER: "Уже есть аккаунт?",
  SIGN_IN: "Войти",
};
export default locale;
