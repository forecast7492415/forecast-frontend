import React from "react";
import "./sign-in-header.css";
import LanguageSwitcher from "../../../../theme-layouts/layout-main/components/LanguageSwitcher";

const SignInHeader = () => {
  return (
    <div className="signInHeader">
      <LanguageSwitcher />
      <div className="container">
        <div>IForecast</div>
      </div>
    </div>
  );
};

export default SignInHeader;
