const locale = {
    AVERAGE_READINGS: "Current readings",
    NAME: "Name",
    VALUE: "Value",
    TIME: "Time"
}

export default locale