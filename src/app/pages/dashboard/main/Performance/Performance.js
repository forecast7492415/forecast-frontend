import React, { useState } from "react";
import { Box, Tab, Tabs } from "@mui/material";
import { tabStyle } from "../../../../utils/stylesForMui";
import { tabsStationStatistics } from "../../../../utils/contstants/tabs";
import MonthOrYearPerformance from "./tab/MonthOrYearPerformance";
import { useSelector } from "react-redux";
import { useTheme } from "@mui/material/styles";
import { useTranslation } from "react-i18next";

const Performance = () => {
  const { mainStationInfo } = useSelector((state) => state.dashboard);
  const [activeTab, setActiveTab] = useState("revenue");
  const { t } = useTranslation("performancePage");
  const theme = useTheme();
  // --------------------------------------------------

  const dataConverters = {
    revenue: (v, type) => (parseInt(v) * 20 * 1000).toFixed(2),
    kium: (v, type) => {
      return (((parseInt(v) / ((type == 'year' ? 30 : 1) * 24 * mainStationInfo.capacity)) * 100)).toFixed(
        2
      )
    },
    emissions: (v, type) => (v * 0.0006).toFixed(2),
  };
  // --------------------------------------------------

  return (
    <div className="container-inner">
      <div className="StationStatistic">
        <h1 className="title">{t("TITLE")}</h1>

        <Box
          sx={{
            borderBottom: 1,
            borderColor: "divider",
            marginBottom: "40px",
            maxWidth: "100vw", // Set maxWidth to 100vw
            overflowX: "auto",
          }}
        >
          <Tabs
            value={activeTab}
            allowScrollButtonsMobile
            variant="scrollable"
            scrollButtons="auto"
            onChange={(_, newValue) => setActiveTab(newValue)}
            sx={{
              "& .MuiTabs-indicator": {
                background: "#F07048",
              },
              // Add the following media query styles
              [theme.breakpoints.down("sm")]: {
                "& .MuiTabs-scroller": {
                  overflowX: "auto",
                },
                "& .MuiTabs-flexContainer": {
                  flexWrap: "nowrap",
                },
              },
            }}
          >
            {tabsStationStatistics.map((tab) => (
              <Tab
                sx={tabStyle}
                key={tab.value}
                value={tab.value}
                label={t(tab.name)}
              />
            ))}
          </Tabs>
        </Box>

        <MonthOrYearPerformance
          dataConverter={dataConverters[activeTab]}
          tabPerformance={activeTab}
        />
      </div>
    </div>
  );
};

export default Performance;
