import {ListItemIcon, Modal, Tab, Tabs} from "@mui/material";
import React, {useEffect, useState} from "react";
import {
    dailyStateCalendar,
    dailyStateCalendarTabCenter,
    dailyStateCalendarTabLeft,
    dailyStateCalendarTabRight,
    datePickerStyle,
} from "../../../../utils/stylesForMui";
import FileDownloadOutlinedIcon from "@mui/icons-material/FileDownloadOutlined";
import CustomTable from "../../../UI/CustomTable/CustomTable";
import Widget from "../../../UI/Widget/Widget";
import "./stats.css";
import {AdapterDateFns} from "@mui/x-date-pickers/AdapterDateFns";
import {LocalizationProvider} from "@mui/x-date-pickers/LocalizationProvider";
import {DatePicker} from "@mui/x-date-pickers/DatePicker";
import CalendarTodayIcon from "@mui/icons-material/CalendarToday";
import {useLocation} from "react-router-dom";
import LineDiagram from "../../../UI/Diagram/LineDiagram";
import BarDiagram from "../../../UI/Diagram/BarDiagram";
import {useDispatch, useSelector} from "react-redux";
import {getData, getDay, getTableData,} from "../../../../store/diagram/diagramSlice";
import ru from "date-fns/locale/ru";
import ModalBase from "../../shared-component/ModalBase/ModalBase";
import FormExport from "../../../UI/FormExport/FormExport";

const Stats = ({statistic}) => {
    // const location = useLocation();
    // const dispatch = useDispatch();
    // // ------------------------------------
    //
    // const {todayStats} = useSelector(
    //     (state) => state.dashboard
    // );
    // // ------------------------------------
    //
    // const [typeData, setTypeData] = useState("chart");
    // const [valueDate, setValueDate] = useState("today");
    // const [showDatePicker, setShowDatePicker] = useState(false);
    // const [selectedDate, setSelectedDate] = useState(new Date());
    // const [selectedYearsAndMonth, setSelectedYearsAndMonth] = useState(new Date());
    // const [selectedYears, setSelectedYears] = useState(new Date());
    // const [open, setOpen] = useState(false);
    // const [openModalExport, setOpenModalExport] = useState(false);
    //
    // const disableDate = (date) =>
    //     date > new Date(Date.now() + 24 * 60 * 60 * 1000);
    //
    // const datePickerChange = (date) => {
    //     setSelectedDate(date);
    // };
    //
    // const handleChange = (_, newValue) => {
    //     setTypeData(newValue);
    //     setShowDatePicker(false);
    //     setValueDate("today");
    // };
    //
    // const handleChangeDate = (_, newValue) => {
    //     if (newValue === "datePicker") {
    //         setShowDatePicker(true);
    //     } else {
    //         setShowDatePicker(false);
    //     }
    //
    //     setValueDate(newValue);
    // };
    //
    // useEffect(() => {
    //     dispatch(getDay());
    //     dispatch(getData());
    //     dispatch(getTableData());
    // }, [statistic]);
    //
    // useEffect(() => {
    //     console.log("Stats selectedDate", selectedDate);
    // }, [selectedDate]);
    //
    // useEffect(() => {
    //     console.log("Stats selectedYearsAndMonth", selectedYearsAndMonth);
    // }, [selectedYearsAndMonth]);
    //
    // useEffect(() => {
    //     console.log("Stats selectedYears", selectedYears);
    // }, [selectedYears]);
    //
    // useEffect(() => {
    //     console.log("Stats valueDate", valueDate);
    // }, [valueDate]);
    //
    // let crumbs = null;
    // if (
    //     statistic === "day" ||
    //     statistic === "temp" ||
    //     statistic === "irrad" ||
    //     statistic === "clouds" ||
    //     statistic === "wind" ||
    //     statistic === "precip"
    // ) {
    //     crumbs = (
    //         <>
    //             <div className="date-block">
    //                 <Tabs
    //                     value={valueDate}
    //                     onChange={handleChangeDate}
    //                     sx={dailyStateCalendar}
    //                 >
    //                     <Tab
    //                         label="Сегодня"
    //                         value={"today"}
    //                         style={{
    //                             backgroundColor: valueDate === "today" ? "#E7EAF2" : "",
    //                         }}
    //                         sx={dailyStateCalendarTabRight}
    //                     />
    //                     <Tab
    //                         label="Завтра"
    //                         value={"tomorrow"}
    //                         style={{
    //                             backgroundColor: valueDate === "tomorrow" ? "#E7EAF2" : "",
    //                         }}
    //                         sx={dailyStateCalendarTabCenter}
    //                     />
    //                     <Tab
    //                         label="Другая дата"
    //                         value={"datePicker"}
    //                         style={{
    //                             backgroundColor: valueDate === "datePicker" ? "#E7EAF2" : "",
    //                         }}
    //                         sx={dailyStateCalendarTabLeft}
    //                     />
    //                 </Tabs>
    //
    //                 {showDatePicker && (
    //                     <div>
    //                         <LocalizationProvider
    //                             dateAdapter={AdapterDateFns}
    //                             adapterLocale={ru}
    //                         >
    //                             <DatePicker
    //                                 sx={datePickerStyle}
    //                                 value={selectedDate}
    //                                 onChange={datePickerChange}
    //                                 shouldDisableDate={disableDate}
    //                                 open={open}
    //                                 onClose={() => setOpen(false)}
    //                                 slotProps={{
    //                                     textField: {
    //                                         onClick: () => setOpen(true),
    //                                     },
    //                                     OpenPickerIcon: CalendarTodayIcon,
    //                                 }}
    //                             />
    //                         </LocalizationProvider>
    //                     </div>
    //                 )}
    //             </div>
    //
    //             <div className="view-block">
    //                 <Tabs
    //                     value={typeData}
    //                     onChange={handleChange}
    //                     sx={dailyStateCalendar}
    //                 >
    //                     <Tab
    //                         label="Диаграмма"
    //                         value={"chart"}
    //                         style={{
    //                             backgroundColor: typeData === "chart" ? "#E7EAF2" : "",
    //                         }}
    //                         sx={dailyStateCalendarTabRight}
    //                     />
    //                     <Tab
    //                         label="Таблица"
    //                         value={"table"}
    //                         style={{
    //                             backgroundColor: typeData === "table" ? "#E7EAF2" : "",
    //                         }}
    //                         sx={dailyStateCalendarTabLeft}
    //                     />
    //                 </Tabs>
    //             </div>
    //         </>
    //     );
    // }
    //
    // return (
    //     <div
    //         className="DailyStats"
    //         style={{
    //             gridTemplateColumns:
    //                 location.pathname === "/user/power-generation" ||
    //                 location.pathname === "/user/station-statistic"
    //                     ? "3fr 1fr"
    //                     : "1fr",
    //         }}
    //     >
    //         {
    //             <>
    //                 <div className="DailyStats__col">
    //                     <div className="DailyStats__header">
    //                         {crumbs}
    //
    //                         {location.pathname === "/user/station-statistic" && (
    //                             <div className="radio-block">
    //                                 <label
    //                                     className={
    //                                         selectedOption === "selectMonth" ? "selected" : "label"
    //                                     }
    //                                 >
    //                                     <input
    //                                         type="radio"
    //                                         value="selectMonth"
    //                                         checked={selectedOption === "selectMonth"}
    //                                         onChange={(e) => setSelectedOption(e.target.value)}
    //                                     />
    //                                     Месяц
    //                                 </label>
    //                                 <label
    //                                     className={
    //                                         selectedOption === "selectYear" ? "selected" : "label"
    //                                     }
    //                                 >
    //                                     <input
    //                                         type="radio"
    //                                         value="selectYear"
    //                                         checked={selectedOption === "selectYear"}
    //                                         onChange={(e) => setSelectedOption(e.target.value)}
    //                                     />
    //                                     Год
    //                                 </label>
    //                             </div>
    //                         )}
    //
    //                         {(statistic === "month" ||
    //                             (location.pathname === "/user/station-statistic" &&
    //                                 selectedOption === "selectMonth")) && (
    //                             <div className="date-block">
    //                                 <LocalizationProvider
    //                                     dateAdapter={AdapterDateFns}
    //                                     adapterLocale={ru}
    //                                 >
    //                                     <DatePicker
    //                                         sx={datePickerStyle}
    //                                         views={["month", "year"]}
    //                                         value={selectedYearsAndMonth}
    //                                         onYearChange={setSelectedYearsAndMonth}
    //                                         minDate={new Date("2021-01-01")}
    //                                         maxDate={new Date()}
    //                                         open={open}
    //                                         onClose={() => setOpen(false)}
    //                                         slotProps={{
    //                                             textField: {
    //                                                 onClick: () => setOpen(true),
    //                                             },
    //                                             OpenPickerIcon: CalendarTodayIcon,
    //                                         }}
    //                                     />
    //                                 </LocalizationProvider>
    //                             </div>
    //                         )}
    //
    //                         {(statistic === "year" || selectedOption === "selectYear") && (
    //                             <div className="date-block">
    //                                 <LocalizationProvider
    //                                     dateAdapter={AdapterDateFns}
    //                                     adapterLocale={ru}
    //                                 >
    //                                     <DatePicker
    //                                         sx={datePickerStyle}
    //                                         views={["year"]}
    //                                         value={selectedYears}
    //                                         onYearChange={setSelectedYears}
    //                                         minDate={new Date("2021-01-01")}
    //                                         maxDate={new Date()}
    //                                         open={open}
    //                                         onClose={() => setOpen(false)}
    //                                         slotProps={{
    //                                             textField: {
    //                                                 onClick: () => setOpen(true),
    //                                             },
    //                                             OpenPickerIcon: CalendarTodayIcon,
    //                                         }}
    //                                     />
    //                                 </LocalizationProvider>
    //                             </div>
    //                         )}
    //
    //                         <button
    //                             className="btn-download"
    //                             onClick={() => setOpenModalExport(true)}
    //                         >
    //                             <ListItemIcon style={{minWidth: "25px"}}>
    //                                 <FileDownloadOutlinedIcon/>
    //                             </ListItemIcon>
    //                             Экспорт таблицы
    //                         </button>
    //
    //                         <Modal
    //                             open={openModalExport}
    //                             onClose={() => setOpenModalExport(false)}
    //                         >
    //                             <ModalBase
    //                                 onClose={() => setOpenModalExport(false)}
    //                                 title={"Экспортировать таблицу"}
    //                             >
    //                                 <FormExport
    //                                     date={selectedDate}
    //                                     month={selectedYearsAndMonth}
    //                                     year={selectedYears}
    //                                 />
    //                             </ModalBase>
    //                         </Modal>
    //                     </div>
    //
    //                     <>
    //                         {typeData === "chart" && (
    //                             <div className="chart">
    //                                 {(statistic === "day" ||
    //                                     statistic === "temp" ||
    //                                     statistic === "irrad" ||
    //                                     statistic === "clouds" ||
    //                                     statistic === "wind" ||
    //                                     statistic === "precip") && (
    //                                     <>
    //                                         {valueDate === "today" && (
    //                                             <LineDiagram
    //                                                 hours={hours}
    //                                                 dataPlan={dataPlan}
    //                                                 dataFact={dataFact}
    //                                             />
    //                                         )}
    //
    //                                         {valueDate === "tomorrow" && (
    //                                             <LineDiagram
    //                                                 day={"tomorrow"}
    //                                                 hours={hours}
    //                                                 dataPlan={dataPlan}
    //                                             />
    //                                         )}
    //
    //                                         {valueDate === "datePicker" && (
    //                                             <LineDiagram
    //                                                 hours={hours}
    //                                                 dataPlan={setData.prognozfakt_graph_his}
    //                                                 dataFact={setData.prognozfakt_graph_fc}
    //                                             />
    //                                         )}
    //                                     </>
    //                                 )}
    //
    //                                 {(statistic === "month" || statistic === "revenue") && (
    //                                     <BarDiagram labels={month} data={dataMonth}/>
    //                                 )}
    //
    //                                 {statistic === "year" && (
    //                                     <BarDiagram labels={years} data={dataYears}/>
    //                                 )}
    //                             </div>
    //                         )}
    //
    //                         {typeData === "table" && (
    //                             <div className="table">
    //                                 {valueDate === "today" && (
    //                                     <>
    //                                         <CustomTable
    //                                             data={tableData}
    //                                             selected={"today"}
    //                                             valueTab={valueTab}
    //                                         />
    //                                     </>
    //                                 )}
    //
    //                                 {valueDate === "tomorrow" && (
    //                                     <>
    //                                         <CustomTable
    //                                             data={tableData}
    //                                             selected={"tomorrow"}
    //                                             valueTab={valueTab}
    //                                         />
    //                                     </>
    //                                 )}
    //
    //                                 {valueDate === "datePicker" && (
    //                                     <>
    //                                         <CustomTable
    //                                             data={tableData}
    //                                             selected={"datePicker"}
    //                                             valueTab={valueTab}
    //                                         />
    //                                     </>
    //                                 )}
    //                             </div>
    //                         )}
    //                     </>
    //                 </div>
    //
    //                 {valueDate !== "tomorrow" &&
    //                     (location.pathname === "/user/power-generation" ||
    //                         location.pathname === "/user/station-statistic") && (
    //                         <div className="DailyStats__col">
    //                             <div className="widgets">
    //                                 {dataWidget.map((el, i) => {
    //                                     return (
    //                                         <Widget
    //                                             key={i}
    //                                             title={el.title}
    //                                             data={el.data}
    //                                             dataName={el.dataName}
    //                                         />
    //                                     );
    //                                 })}
    //                             </div>
    //                         </div>
    //                     )}
    //             </>
    //
    //         }
    //     </div>
    // );

    return (
        <></>
    );
};

export default Stats;
