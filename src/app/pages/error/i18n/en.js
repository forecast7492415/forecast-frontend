const locale = {
  NOT_FOUND: "Page not found",
  DESCRIPTION: "The page you are looking for does not exist.",
  GO_HOME: "Go to home",
};
export default locale;
