const locale = {
  NOT_FOUND: "Веб-парақ табылмады",
  DESCRIPTION: "Сіз іздеген бет табылмады",
  GO_HOME: "Бастапқы бетке оралу",
};
export default locale;
