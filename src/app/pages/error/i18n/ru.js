const locale = {
  NOT_FOUND: "Страница не найдена",
  DESCRIPTION: "Страница, которую вы ищете, не существует.",
  GO_HOME: "Вернуться на главную",
};
export default locale;
