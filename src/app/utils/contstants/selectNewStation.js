export const selectNewStation = [
  { value: "abayskays", label: "REGIONS.AKMOLA" },
  { value: "akmolinskaya", label: "REGIONS.ABAY" },
  { value: "aktubinskaya", label: "REGIONS.AKTOBE" },
  { value: "almatynskaya", label: "REGIONS.ALMATY" },
];
