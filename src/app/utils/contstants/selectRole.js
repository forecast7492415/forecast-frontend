export const selectRole = [
  { value: "Director", label: "FORM_MODAL.DIRECTOR" },
  { value: "Engineer", label: "FORM_MODAL.ENGINEER" },
  { value: "Operator", label: "FORM_MODAL.OPERATOR" },
  { value: "Dispatcher", label: "FORM_MODAL.DISPATCHER" },
];
